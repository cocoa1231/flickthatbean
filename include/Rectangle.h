#ifndef RECTANGLE_H
#define RECTANGLE_H

#include <SFML/Graphics.hpp>

class Rectangle
{
    public:
        Rectangle(sf::Vector2f size);
        void setPosition(sf::Vector2f newPos);
        int drawTo( sf::RenderWindow &window );

    private:
        sf::RectangleShape rectangle;
};

#endif // RECTANGLE_H
