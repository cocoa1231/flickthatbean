#ifndef TARGET_H
#define TARGET_H
#include <SFML/Graphics.hpp>

class Target
{
    public:
        Target();
        void setPosition(sf::Vector2f position);
        int drawTo( sf::RenderWindow &window );
        sf::FloatRect GetGlobalBounds();

    private:
        sf::CircleShape target;
};

#endif // TARGET_H
