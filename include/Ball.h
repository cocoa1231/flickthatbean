#ifndef BALL_H
#define BALL_H

#include <SFML/Graphics.hpp>
#include "Target.h"
class Ball
{
    public:
        Ball(float radius, unsigned int pointCount);
        void setPosition(sf::Vector2f newPosition);
        int drawTo(sf::RenderWindow &window);
        bool isCollidingWith(Target target);

    private:
        sf::CircleShape ball;
};

#endif // BALL_H
