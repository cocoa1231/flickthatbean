#include <iostream>
#include <math.h>
#include <string>
#include <random>
#include <fstream>
#include "SFML/Graphics.hpp"
#include "Ball.h"
#include "Rectangle.h"
#include "Target.h"

struct StorePoints
{
    std::string  Name;
    float points;
};

float retRandom(int min, int max)
{
    std::random_device rd;                           // only used once to initialise (seed) engine
    std::mt19937 rng(rd());                          // random-number engine used (Mersenne-Twister in this case)
    std::uniform_int_distribution<int> uni(min,max); // guaranteed unbiased

    auto random_integer = uni(rng);

    return random_integer;
}

int main()
{
    std::string name;
    std::cout << "Enter your name: ";
    std::cin >> name;

    StorePoints Player = { name, 0 };

    // Create the main window
    sf::RenderWindow app(sf::VideoMode(800, 800), "SFML window");

    // Some dimentions
    float radius = 10.0;
    unsigned int pointCount = 30;

    // Constants
    std::string angle = "0";
    int score = 0;
    int cnt   = 0;

    // Get x value of line and ball

    float position = retRandom(0, 700);
    float target_position = retRandom(0, 700);

    // Create a Ball Object
    Ball mainBall(radius, pointCount);
    mainBall.setPosition({ position-radius, 600 });

    // Create center line
    Rectangle line({1, 800});
    line.setPosition({position, 0});

    // Create target
    Target targ;
    targ.setPosition({target_position, 100});

	// Start the game loop
    while (cnt < 2)
    {
        // Process events
        sf::Event event;
        while (app.pollEvent(event))
        {
            // Close window : exit
            if (event.type == sf::Event::Closed)
                app.close();
            if (event.type == sf::Event::TextEntered)
            {
                if (event.text.unicode == 8)
                {
                    try
                    {
                        angle.pop_back();
                        std::cout << "ASCII character typed: " << angle << std::endl;
                    }
                    catch (...)
                    {
                        std::cout << "It's deleted" << std::endl;
                    }
                }
                else if (event.text.unicode > 47 && event.text.unicode < 58 || event.text.unicode == 45)
                {

                    angle += static_cast<char>(event.text.unicode);
                    std::cout << "ASCII character typed: " << angle << ", " << event.text.unicode << std::endl;
                }

            }
            else if (event.type == sf::Event::KeyPressed)
            {
                switch (event.key.code)
                {
                    case (sf::Keyboard::Return):
                    {
                        double d_angle = std::stod(angle);
                        double displacement_x = std::tan(d_angle) * 500;
                        mainBall.setPosition({ displacement_x, 100 });
                        std::cout << "New Position: " << displacement_x << ", " << 100 << std::endl;
                        std::cout << d_angle << std::endl;
                        if (mainBall.isCollidingWith(targ))
                            Player.points = score++;
                        cnt++;
                        std::cout << cnt << std::endl;
                    }
                    case (sf::Keyboard::R):
                    {
                        mainBall.setPosition({position, 600});
                    }
                }
            }

        }

        std::ofstream out("highscore.txt");
        out << Player.Name + ", ";
        out << std::to_string(Player.points) + "\n";
        out.close();
        // Clear screen
        app.clear();

        // Draw Stuff
        mainBall.drawTo(app);
        line.drawTo(app);
        targ.drawTo(app);

        // Update the window
        app.display();
    }

    return EXIT_SUCCESS;
}
