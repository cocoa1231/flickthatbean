#include "Rectangle.h"

Rectangle::Rectangle( sf::Vector2f size )
{
    rectangle.setSize( size );
}

void Rectangle::setPosition( sf::Vector2f newPos )
{
    rectangle.setPosition(newPos);
}

int Rectangle::drawTo( sf::RenderWindow &window )
{
    window.draw(rectangle);
    return 0;
}

