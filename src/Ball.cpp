#include "Ball.h"
#include "Target.h"
Ball::Ball(float radius, unsigned int pointCount)
{
    ball.setRadius(radius);
    ball.setPointCount(pointCount);
    ball.setFillColor(sf::Color::Green);
}

int Ball::drawTo( sf::RenderWindow &window )
{
    window.draw(ball);
    return 0;
}

void Ball::setPosition(sf::Vector2f newPosition)
{
    ball.setPosition(newPosition);
}

bool Ball::isCollidingWith(Target target)
{
    if (ball.getGlobalBounds().intersects(target.GetGlobalBounds())) {
        return true;
    }
    return false;
}
