#include "Target.h"

Target::Target()
{
    target.setRadius(30.0);
    target.setFillColor(sf::Color::Red);
}

void Target::setPosition( sf::Vector2f position )
{
    target.setPosition(position);
}

int Target::drawTo( sf::RenderWindow &window )
{
    window.draw(target);
}


sf::FloatRect Target::GetGlobalBounds()
{
    return target.getGlobalBounds();
}
